import asyncio
import os

import aiohttp

WS_DOMAIN = os.getenv("WS_DOMAIN", "localhost")
WS_HOST = os.getenv("WS_HOST", "0.0.0.0")
WS_PORT = int(os.getenv("WS_PORT", 8888))


async def fetch_wsurl(session, url, headers=None):
    async with session.get(url, headers=headers) as response:
        json = await response.json()
        wsurl = json["url"]
        return wsurl


async def main():
    session = aiohttp.ClientSession()
    ws_url = await fetch_wsurl(
        session,
        f"http://{WS_DOMAIN}:{WS_PORT}/api/rtm.connect",
        headers={"Authorization": "Token Wcw33RIhvnbxTKxTxM2rKJ7YJrwyUXhXn"},
    )
    async with session.ws_connect(ws_url) as ws:
        print(f"Client connected to {ws_url}")
        async for msg in ws:
            print("Message received: ", msg.data)

            if msg.type in (aiohttp.WSMsgType.CLOSED, aiohttp.WSMsgType.ERROR):
                break


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())