import asyncio
import os
import secrets
import weakref

import aiohttp.web
import aioredis
import async_timeout
from aiohttp import WSCloseCode

WS_DOMAIN = os.getenv("WS_DOMAIN", "localhost")
WS_HOST = os.getenv("WS_HOST", "0.0.0.0")
WS_PORT = int(os.getenv("WS_PORT", 8888))


async def ws_handler(request):
    channel_id = request.match_info.get("channel_id")
    ws = aiohttp.web.WebSocketResponse()
    await ws.prepare(request)
    # 我們直接將連線物件 reference 存放在程式記憶體區塊中
    request.app["websockets"][channel_id] = ws

    print("Client connected: ", channel_id)

    try:
        await asyncio.gather(
            listen_to_websocket(ws), listen_to_redis(request.app, channel_id)
        )
    finally:
        request.app["websockets"].pop(channel_id, None)
        print("Websocket connection closed")



async def wsticket_handler(request):
    # 實作可在此檢查認證資訊, 通過後產生一個時效性 ticket, 儲存至認證用快取/資料庫等, 供其他服務查閱
    # 最後回覆 websocket 的時效性 url

    ws_url = (
        f"ws://{WS_DOMAIN}:{WS_PORT}/ws/{secrets.token_urlsafe(32)}"
    )  # 生產環境應為加密的 wss
    return aiohttp.web.json_response({"url": ws_url})


async def wspush_handler(request):
    channel_id = request.match_info.get("channel_id")
    ch_name = f"ch:{channel_id}"
    data = await request.text()
    conn = await request.app["redis_pool"]
    await conn.publish(ch_name, data)

    print(f"message {data} pushed to {ch_name}")

    raise aiohttp.web.HTTPOk()


async def listen_to_websocket(ws):
    try:
        async for msg in ws:
            # 可在此實作從 client 接收訊息時的處理邏輯
            pass
    finally:
        return ws


async def listen_to_redis(app, channel_id):
    pubsub = app["redis_pool"].pubsub()
    ch_name = f"ch:{channel_id}"
    ws = app["websockets"][channel_id]
    await pubsub.subscribe(ch_name)
    print("Channel created: ", ch_name)

    async with pubsub:
        while True:
            try:
                async with async_timeout.timeout(1):
                    message = await pubsub.get_message(ignore_subscribe_messages=True)
                    if message is not None:
                        print(f"(Reader) Message Received: {message}")
                        await ws.send_str(str(message.get("data")))
                    await asyncio.sleep(0.01)
            except (asyncio.CancelledError, asyncio.TimeoutError):
                pass
    await pubsub.unsubscribe(ch_name)


async def on_startup(app):
    # 建立 Redis connection pool
    app["redis_pool"] = await aioredis.from_url("redis://140.125.207.230:6379?encoding=utf-8", encoding="utf-8", decode_responses=True)


async def on_shutdown(app):
    # 關閉所有 websockets 與 Redis 連線
    # Ignore 2.0.0 bug for now: <RuntimeWarning: coroutine 'Redis.close' was never awaited>
    #   https://github.com/aio-libs/aioredis-py/issues/1086
    await app["redis_pool"].close()
    for ws in app["websockets"].values():
        await ws.close(code=WSCloseCode.GOING_AWAY, message="Server shutdown")
    

@aiohttp.web.middleware
async def auth_middleware(request, handler):
    if request.path.startswith("/api/"):
        pass
        # Token 驗證:
        # 驗證失敗:
        # raise aiohttp.web.HTTPForbidden()
    return await handler(request)


def main():
    app = aiohttp.web.Application(middlewares=[auth_middleware])
    # 建立一個 reference dict 準備關聯全部 ws 連線物件, key 為 {channel_id}
    app["websockets"] = weakref.WeakValueDictionary()
    app.router.add_get("/ws/{channel_id}", ws_handler)
    app.router.add_get("/api/rtm.connect", wsticket_handler)
    app.router.add_post("/api/rtm.push/{channel_id}", wspush_handler)
    app.on_startup.append(on_startup)
    app.on_shutdown.append(on_shutdown)
    aiohttp.web.run_app(app, host=WS_HOST, port=WS_PORT)


if __name__ == "__main__":
    main()